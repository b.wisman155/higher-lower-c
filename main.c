#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    bool guessed = false;

    printf("Welcome to higher/lower\n");

    srand(time(NULL));
    int rand_num = rand() % 100;

    int number;
    
    while (!guessed) {
        printf("enter number: ");
        scanf("%d", &number);

        if (number > rand_num) {
            printf("It needs to be lower\n\n");
        } else if (number < rand_num) {
            printf("It needs to be higher\n\n");
        } else {
            printf("YOU GOT IT, IT WAS %d\n", rand_num);
            return 0;
        }
    }
}
